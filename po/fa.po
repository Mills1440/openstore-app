# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-31 04:27+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.8\n"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:114
#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:33
msgid "App details"
msgstr "جزئیات برنامه"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:121
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:386
#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:40
#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:33
msgid "Remove"
msgstr "برداشتن"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:157
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:17
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:62
msgid "App"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:158
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:50
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:18
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:63
msgid "Scope"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:159
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:19
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:64
msgid "Web App"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:160
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageListItem.qml:20
#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:65
msgid "Web App+"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:240
msgid ""
"The installed version of this app doesn't come from the OpenStore server. "
"You can install the latest stable update by tapping the button below."
msgstr ""
"نسخه نصب شده این برنامه از سرور OpenStore تهیه نمی شود. با ضربه زدن به دکمه "
"زیر می توانید آخرین بروزرسانی پایدار را نصب کنید."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:265
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HighlightedApp.qml:109
msgid "Open"
msgstr "بازکردن"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:281
msgid "The OpenStore is installed!"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:290
msgid "Install stable version"
msgstr "نصب نسخه پایدار"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:293
msgid "Upgrade"
msgstr "ارتقأ"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:296
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:358
msgid "Install"
msgstr "نصب"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:372
msgid "This app is not compatible with your system."
msgstr "این برنامه با سیستم شما سازگار نیست."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:405
msgid ""
"This app has access to restricted parts of the system and all of your data, "
"see below for details."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:408
msgid "This app has access to restricted system data, see below for details."
msgstr ""
"این برنامه به داده های سیستم محدود دسترسی دارد ، برای جزئیات بیشتر به زیر "
"مراجعه کنید."

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:433
msgid "Description"
msgstr "شرح"

#. TRANSLATORS: Title of the changelog section
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:466
msgid "What's New"
msgstr "چه خبر"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:485
msgid "Packager/Publisher"
msgstr "بسته / ناشر"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:486
msgid "OpenStore team"
msgstr "تیم OpenStore"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:494
#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:115
msgid "Installed version"
msgstr "نسخه نصب شده"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:504
msgid "Latest available version"
msgstr "آخرین نسخه موجود"

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:513
msgid "First released"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:524
msgid "Downloads of the latest version"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:533
msgid "Total downloads"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:542
msgid "License"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:543
msgid "N/A"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:553
#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:215
msgid "Source Code"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:566
msgid "Get support for this app"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:579
msgid "Donate to support this app"
msgstr ""

#. TRANSLATORS: This is the button that shows a list of all the packages from the same author. %1 is the name of the author.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:595
#, qt-format
msgid "More from %1"
msgstr ""

#. TRANSLATORS: This is the button that shows a list of all the other packages in the same category. %1 is the name of the category.
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:610
#, qt-format
msgid "Other apps in %1"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:616
msgid "Package contents"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:709
msgid "Permissions"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:714
msgid "Accounts"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:715
msgid "Audio"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:716
msgid "Bluetooth"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:717
msgid "Calendar"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:718
msgid "Camera"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:719
msgid "Connectivity"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:720
msgid "Contacts"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:721
msgid "Content Exchange Source"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:722
msgid "Content Exchange"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:723
msgid "Debug"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:724
msgid "History"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:725
msgid "In App Purchases"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:726
msgid "Keep Display On"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:727
msgid "Location"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:728
msgid "Microphone"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:729
msgid "Read Music Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:730
msgid "Music Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:731
msgid "Networking"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:732
msgid "Read Picture Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:733
msgid "Picture Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:734
msgid "Push Notifications"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:735
msgid "Sensors"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:736
msgid "User Metrics"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:737
msgid "Read Video Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:738
msgid "Video Files"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:739
msgid "Video"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:740
msgid "Webview"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:744
msgid "Full System Access"
msgstr ""

#. TRANSLATORS: this will show when an app doesn't need any special permissions
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:749
msgid "none required"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:785
msgid "Read paths"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:802
msgid "Write paths"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:817
msgid "Donating"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:818
msgid "Would you like to support this app with a donation to the developer?"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:824
msgid "Donate now"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:832
msgid "Maybe later"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:845
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:297
msgid "Warning"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:846
msgid ""
"This app has access to restricted parts of the system and all of your data. "
"It has the potential break your system. While the OpenStore maintainers have "
"reviewed the code for this app for safety, they are not responsible for "
"anything bad that might happen to your device or data from installing this "
"app."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:852
msgid "I understand the risks"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:861
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:366
#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:82
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:198
#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:42
msgid "Cancel"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:880
#, qt-format
msgid "%1 GB"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:885
#, qt-format
msgid "%1 MB"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:890
#, qt-format
msgid "%1 kB"
msgstr ""

#. TRANSLATORS: %1 is the size of a file, expressed in bytes
#: /home/brian/dev/openstore/openstore-app/openstore/AppDetailsPage.qml:893
#, qt-format
msgid "%1 bytes"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:95
msgid "This app could not be found in the OpenStore"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/AppLocalDetailsPage.qml:98
msgid ""
"You are currently offline and the app details could not be fetched from the "
"OpenStore"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/CategoriesTab.qml:38
msgid "Categories"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:65
#, qt-format
msgid "by %1"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:98
msgid "OpenStore update available"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:134
#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:135
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:51
msgid "Installed Apps"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:134
#, qt-format
msgid " (%1 update available)"
msgid_plural " (%1 updates available)"
msgstr[0] ""
msgstr[1] ""

#: /home/brian/dev/openstore/openstore-app/openstore/DiscoverTab.qml:159
msgid "Browse Apps by Category"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "Nothing here yet"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:106
msgid "No results found."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "No app has been released in this category yet."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/FilteredAppView.qml:107
msgid "Try with a different search."
msgstr ""

#. TRANSLATORS: %1 is the number of installed apps
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:79
#, qt-format
msgid "Installed apps (%1)"
msgstr ""

#. TRANSLATORS: %1 is the number of available app updates
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:83
#, qt-format
msgid "Available updates (%1)"
msgstr ""

#. TRANSLATORS: %1 is the number of apps that can be downgraded
#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:87
#, qt-format
msgid "Stable version available (%1)"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:90
msgid ""
"The installed versions of these apps did not come from the OpenStore but a "
"stable version is available."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:91
msgid "Update all"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:193
msgid "No apps found"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/InstalledAppsTab.qml:194
msgid "No app has been installed from OpenStore yet."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:306
msgid ""
"You are currently using a non-standard domain for the OpenStore. This is a "
"development feature. The domain you are using is:"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:320
msgid "Are you sure you want to continue?"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:324
msgid "Yes, I know what I'm doing"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:332
msgid "Get me out of here!"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:344
msgid "Install unknown app?"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:345
#, qt-format
msgid "Do you want to install the unknown app %1?"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:386
msgid "App installed"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:387
msgid "The app has been installed successfully."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:390
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:403
#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:418
msgid "OK"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:399
msgid "Installation failed"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:400
msgid ""
"The package could not be installed. Make sure it is a valid click package."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Main.qml:414
#, qt-format
msgid "Installation failed (Error %1)"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:29
msgid "Your passphrase is required to access restricted content"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:30
msgid "Your passcode is required to access restricted content"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:39
msgid "Authentication failed"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passphrase required"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:47
msgid "Passcode required"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passphrase (default is 0000)"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:59
msgid "passcode (default is 0000)"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:71
msgid "Authentication failed. Please retry"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/PasswordDialog.qml:76
msgid "Authenticate"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:16
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HeaderMain.qml:39
msgid "Search"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SearchTab.qml:29
msgid "Search in OpenStore..."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:27
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HeaderMain.qml:26
msgid "Settings"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:114
msgid "OpenStore Account"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:122
#: /home/brian/dev/openstore/openstore-app/openstore/SignInWebView.qml:30
msgid "Sign in"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:139
msgid "Sign out"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:167
msgid "Parental control"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:173
msgid "Hide adult-oriented content"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:183
msgid ""
"By typing your password you take full responsibility for showing NSFW "
"content."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:208
msgid "About OpenStore"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:224
msgid "Report an issue"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/SettingsPage.qml:233
msgid "Additional Icons by"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:84
msgid "Back"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/BottomEdgePageStack.qml:84
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:83
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:97
msgid "Close"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HighlightedApp.qml:106
#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:187
msgid "Update"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HighlightedApp.qml:112
msgid "Details"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:37
msgid "Application"
msgstr ""

#. TRANSLATORS: This is an Ubuntu platform service for launching other applications (ref. https://developer.ubuntu.com/en/phone/platform/guides/url-dispatcher-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:64
msgid "URL Handler"
msgstr ""

#. TRANSLATORS: This is an Ubuntu platform service for content exchange (ref. https://developer.ubuntu.com/en/phone/platform/guides/content-hub-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:78
msgid "Content Hub Handler"
msgstr ""

#. TRANSLATORS: This is an Ubuntu platform service for push notifications (ref. https://developer.ubuntu.com/en/phone/platform/guides/push-notifications-client-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:92
msgid "Push Helper"
msgstr ""

#. TRANSLATORS: i.e. Online Accounts (ref. https://developer.ubuntu.com/en/phone/platform/guides/online-accounts-developer-guide/ )
#: /home/brian/dev/openstore/openstore-app/openstore/Components/HookIcon.qml:106
msgid "Accounts provider"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:81
msgid "Update available"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/PackageTile.qml:81
msgid "✓ Installed"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:73
msgid "Something went wrong..."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:79
msgid "Error Posting Review"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:93
msgid "Review Posted Correctly"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:94
msgid "Your review has been posted successfully."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:108
msgid "Rate this app"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:108
msgid "Loading..."
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:129
msgid "(Optional) Write a review"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:133
#, qt-format
msgid "%1/%2 characters"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:222
#, qt-format
msgid "%1 review. "
msgid_plural "%1 reviews. "
msgstr[0] ""
msgstr[1] ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:228
msgid "Sign in to review this app"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:229
msgid "Review app"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/ReviewPreview.qml:230
msgid "Install this app to review it"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:46
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:54
#: /home/brian/dev/openstore/openstore-app/openstore/Components/TextualButtonStyle.qml:60
msgid "Pick"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:25
msgid "Remove package"
msgstr ""

#: /home/brian/dev/openstore/openstore-app/openstore/Components/UninstallPopup.qml:26
#, qt-format
msgid "Do you want to remove %1?"
msgstr ""
